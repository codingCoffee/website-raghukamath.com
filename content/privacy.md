---
title: Privacy
type: page
layout: static
---

# Terms of Service and Privacy Policy

This is my personal website and blog, this page is to provide information about
data collection policies and terms of services of this website to be compliant
with [General Data Protection
Regulation](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
Act of Europian Union. Currently, this website is hosted on a server provided
by [Hetzner](https://hetzner.com), the server is located in Nuremberg, Germany.

The following information applies to the following domains that I own and operate.
 
 - raghukamath.com
 - emblik.studio

## Information about the author of this website

 - Raghavendra Kamath
 - Country - India
 - Mail - raghu@raghukamath.com

## Information about the website

The domains are provided by [Godaddy](https://in.godaddy.com/) and
[Gandi](http://gandi.net/), and as mentioned above the server is hosted on
[Hetzner](https://hetzner.com), please refer to their websites to read their
privacy policy and data collection policies, as they may collect data about
you.

This website is built by using a static site generator called
[Hugo](https://gohugo.io/), the source code for the website is available for
inspection [here](https://gitlab.com/raghukamath/website-raghukamath.com).
I do not use any third party analytics or trackers like google analytics etc.
If in future I decide to use any analytics to analyze traffic coming to the
website, it will be hosted on my own server and this document will be updated
to inform you about the data that is being collected. I also do not use any
content delivery network services to serve fonts, images or videos on my
website. If in any of my blog posts, I embed a video hosted by a third party, I
will mention it on that blog post and link you to the privacy policy of the
service, as they may collect information about you.

The commenting service on this website is made possible by an open source self
hosted commenting system called [commento](https://www.commento.io). This
service is hosted on my own server and no third party has access to this.

## Information about the Data collected

### Comments

When you login to make the comment on any of my blog post or articles, you
will be prompted to provide an email address, name, and password. In addition
to this, your IP address is stored in server logs when you access the website.
I store this data in a database running on a server (commento.emblik.studio).
Your email address and other data will not be shared with any external entity
or used for any other purpose. The email information is required to
authenticate you and stop spam comments. However, you can choose to comment
anonymously without providing any details. In such a scenario, the only thing
that is stored is your IP address used to access the website.

### Cookies

When you choose to authenticate yourself to comment, a unique randomly
generated token ("Session") will be stored as a cookie in your browser in order
to remember you on future visits. If you are not logged in, no cookies will be
stored in your browser

### Spam service by Akismet

The commenting system that I use on this website uses [Akismet](https://akismet.com/)
automatic spam filtering, they may store and use some of your data for spam filteration
process. I request you to read their privacy policy too.

## Data protection

The log files which record your IP address and the data collected for the
commenting system are stored on a server which only I can access. In an
unfortunate event of a data breach, your email and password may leak to the
hacker, so I suggest not using the same password you use elsewhere, to make an
account here. I would try to mitigate the breach in a transparent and best way
possible to me. If you notice any misuse or breach of data, you can immediately
contact me via my email address - raghu@raghukamath.com. If you want the data
associated to you, like comments email etc to be deleted, please mail me. If it
is possible to export and segregate your data, I may send you your data on
request via mail. However I would need some way to authenticate that the data
actually belongs to you unless I am sure, I won't oblige to the request.

Thank you for taking time to read this, if you have any more questions please
feel free to email me - raghu@raghukamath.com.
