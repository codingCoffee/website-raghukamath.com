---
title: talks
type: page
layout: static
---

## Some of my talks about working in creative field with FOSS tools

- [FlOSS in Art](https://raghukamath.com/talks/floss-in-art/) - ILUG - Mumbai, Maharashtra.
- [Commercial Art With Libre Software](https://raghukamath.com/talks/swatantra-17/) - Swatantra 2017 - Trivandrum, Kerala.

Please mail me at raghu at raghukamath dot com for enquiries regarding a talk of seminars.
