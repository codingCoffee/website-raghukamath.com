+++
title = "New year new journey"
date = "2015-12-30T18:52:00+05:30"
tags = ["sketches"]
thumb = "/img/newproject-thumb.jpg"
description = "Some WIP sketches of my new project. I have done some initial research and brainstorming for the concept, I'll be uploading the sketches here soon"
keywords = ["sketches", "krita", "floss digital painting"]
slug = "new-year-new-journey"
+++

From some time I have been contemplating to start a new project, since
everybody is in a mood of undertaking new year resolutions. I think it
is time for me to take one too, and I think a resolution to kickstart
and build a base for this new project would be good. I have done some
initial research and brainstorming for the concept, I'll be uploading
the sketches here soon.

Writing a story from scratch is new to me, I am a bit nervous and I also
fear that I may ruin the concept by over thinking it, but then I should
start somewhere. Although it is hard to write for me I am enjoying this
process.Below are two sketches from the project. I'll be uploading more
as and when they get clearer to me.

![sketches raghukamath](/img/sketches/sketch1.jpg)

![sketches raghukamath](/img/sketches/sketch2.jpg)

I hope to do some illustrations on this project at-least a number which
can set the story in motion :)
