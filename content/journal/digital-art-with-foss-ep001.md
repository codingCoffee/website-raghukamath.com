+++

title = "Digital art with foss episode 1"
date = "2018-04-22T15:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials" ]
thumb = "/img/videos-thumbs/daef-thumb-01.png"
description = "This is the first episode in the series on digital art with free and open source software. This episode covers the basics of painting in krita."
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita" ]
slug = "digital-art-with-foss-espisode-1"

+++

This series is about creating digital art in free software. For those
who don't know what free software is please read
[this](https://en.wikipedia.org/wiki/Free_software). This is the first
episode in the series on basics of painting in
[krita.](https://krita.org)

{{< video src="/videos/Digital-art-with-floss-krita-ep01.mp4" poster="video-thumb-ep01.png"  type="mp4" >}}

[Download the video](/videos/Digital-art-with-floss-krita-ep01.mp4)

## Episode Notes

Krita is a free and open source digital painting application. We take a
tour of Krita's interface and learn how to create a new document in
Krita.
