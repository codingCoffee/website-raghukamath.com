+++

title = "Switch To freedom"
date = "2015-11-30T19:41:00+05:30"
tags = ["open source"]
thumb = "/img/switchto-freedom-thumb.jpg"
description = "My experience of switching from proprietary software to a free and open source workflow."
keywords = ["open source", "Krita", "GIMP", "India", "floss painting", "digital painting with Krita", "Inkscape", "Imagemegick artists"]
slug = "switch-to-freedom"

+++

<blockquote class="quote">
"A foolish person only learns about fire when he burns himself".
</blockquote>

This is a phrase in my mother tongue, After being burnt by closed source
software I finally made the switch to Free and Open source
Software. This is but this particular experience is new to me and I felt
like I should share it.

In 2009, after much research and reading countless reviews, I bought a
Dell laptop for my sister. I had purchased a laptop without an OS on it.
Well it came with basic Free-DOS installed on it.

I got the delivery at night, and like any other gadget that I bought I
was excited and started to set it up without wasting any time. I had
downloaded a Linux distribution called "Ubuntu" beforehand.

Unless you muster up your will and plunge into the water. You won't
learn how to swim. Without any deep knowledge of Linux, I installed it
slowly by reading guides and help forums. My internet connection was not
good since my neighbourhood had not yet dreamt of Fiber or broadband, I
downloaded and finished installing the OS till 6 AM with good PPP and
dialup connection from local ISP.

I was greeted by the purplish and orange wallpaper, I poked around a bit
and I liked it. It was working great, I wondered how can someone give a
whole Operating System for free and that too with all the tools that are
needed for daily task without even charging a penny. My sister liked it
too. She still uses that laptop with Ubuntu 14.10 on it.

I was smitten by the idea and philosophy of Libre software and having
complete control over your system and not having hardware upgrades
forced on me was cherry on top of the cake. But as a professional
Illustrator and designer, I couldn't justify the switch. My heart was
telling me to take a leap of faith but my brain was rejecting the idea,
because of lack of software for illustration and graphic design on
Linux. I knew about gimp, but I had no time to adjust to a new work flow
and some other workarounds were time-consuming. This was my initial
assessment as a novice.

## The Breaking Point

I had Windows 7 on my desktop workstation it got upgraded to windows 8,
I had Adobe's Creative Cloud subscription and my work flow was near
perfect. Then one day I read somewhere on Adobe's forum, that if the
Creative cloud subscription ends one couldn't open their software. Which
sounded fair to me, no rent no key to the house that's the analogy I
thought in my mind. But if you can't even open the software then you
can't open or view thousands of .PSD .FLA .AI you have painstakingly
created all these years. It was in fact, no rent no key to the house
along with your belongings that are within the house. How can I justify
paying rent to get access to my own creations, this idea seemed stupid
to me. Some people on the online forums suggested that I should stick to
cs6 but that will be outdated soon and I can't rely on it.

Also, Windows and Photoshop was being updated with new options and
features continuously, to run them I needed to upgrade my hardware with
more ram more G.P.U at a faster rate.

## The Switch

![switch to freedom - Linux Krita](/img/freedom.png)

Then one day, I read a post from a French Illustrator named
[David Revoy](http://www.davidrevoy.com/article170/the-choice-of-open-source).
He had written why he chose open source software, that post helped me
clear my mind. Now my mind and heart were all singing in sync.

I immediately started preparing for the transition. Most of the open
source applications that I found suitable for my work flow also had
windows executable, I downloaded them and tested them on windows
beforehand. I slowly converted all the files to open format wherever
required, I converted my Photoshop brush tips to pngs, I converted my
color swatches, shapes and other assets for the switch.

Now some of you might wonder if the open source application had windows
packages then why am I making a switch to Linux. The answer is
fine-grained control over the hardware and OS itself. I have used my
sister's laptop throughout these years for developing some android apps
with her and I loved the freedom of choice it gave. It felt like I
actually owned the machine which I can inspect and change to my hearts
content. The OS never came in my way or was in any way hindering my
workflow.

So once again I took a deep breath and installed (dual booted) Ubuntu
14.04 on my main desktop.

## Krita

Everything went smoothly I installed all the application that I
required, and I was back in business. One particular application that
had a great impact on me was Krita. The application is so good that it
feels like it is made keeping my workflow in mind. I read about it on
the internet and the more I read the more I got influenced. I joined the
forum and joined the [IRC](https://krita.org/irc/). The experience was
overwhelming. I had never seen a software developer who actually asks
the end user for new features and improvements and listen to them
seriously. Their goal is simply making the best free digital painting
application. And I think, they have already achieved that goal. The
brush engine is the best, there are so many more features and
improvements for my workflow that I cannot write it all here.

I suggest you to [download](https://krita.org/download/krita-desktop/)
it and use it yourself to know how good it is. I also warn you that is
not jack of all trades like Photoshop but a master application only
meant for painting and creating original artworks.

Other software that I use are [Inkscape](https://inkscape.org/en/),
[Mypaint](http://mypaint.intiLinux.com/),
[Blender](http://www.blender.org/),
[Scribus](http://wiki.scribus.net/canvas/Download),
[Gimp](http://www.gimp.org/downloads/),
[Libreoffice](https://www.libreoffice.org/),
[Xsane](http://www.xsane.org/). All of these are free to use.

## Result

Ever since I made the switch, I have never encountered a single problem.
I share files as .psd (Krita supports PSD) or .tiff or .EPS. I have a
[Cron](http://en.wikipedia.org/wiki/Cron) job set up for backing up my
entire work and home folder. Even if my hard drive crashes I have the
backup which also includes all the settings and assets of my software. I
just need to install the OS and restore the backup. All my files and
favourites brushes, swatches etc are there. I can do whatever I want
with my system, My OS works below 1gb of ram without coming in my way,
without all those update popups or restarts popups. If I want some
software all I have to do is write "sudo apt-get install <software
name>" in terminal and boom! I have it installed it's that flexible. If
I don't like the way Ubuntu is, I have a choice of hundreds of other
[Linux distributions](http://distrowatch.com/) to try.

I am glad that I made the switch, it's been wonderful journey so far,
and I am sure that it will be that way :)
