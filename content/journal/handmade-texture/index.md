+++
title = "Seamless Handmade Textures"
date = "2019-11-30T12:17:00+05:30"
tags = ["open source", "textures", "resources", "cc-by-sa"]
thumb = "/journal/handmade-texture/handmade-texture-thumb-600.png"
description = "Sharing some of the handmade texture that I created with Krita while working on various projects"
keywords = "Krita, handmade texture, texture pack, krita, linux, cc-by, free"
+++


![Seamless handmade texture](/journal/handmade-texture/handmade-texture-hero.png)

I am sharing these free seamless handmade or hand-painted textures.These were made while I was working on various projects. Not all are hand-painted some are scanned and prepared from the original item, for example, the cartridge texture is a scan of a paper, some might have made by using resources in the public domain. These are shared under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license, so you are free to reuse modify or share these. I have added all these to a Gitlab [repository](https://gitlab.com/raghukamath/Handmade-textures) and I'll be updating and adding more to this repository as and when I make a new texture. I also welcome any contribution to this repository. You can download the entire repository along with the textures and their source files from [here](https://gitlab.com/raghukamath/Handmade-textures/-/archive/master/Handmade-textures-master.zip). If you want to download individual textures click on the thumbnails below.

<div class="grid-of-three">

<div class="work-post">
<p class="raw-para"> <a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/001-cement-texture-hires.png"><img src="/journal/handmade-texture/001-cement-texture.png" alt="free seamless cement texture"></a>
Cement plaster texture</p>
</div>

<div class="work-post">
<p class="raw-para"><a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/002-brick-seamless-hires.png"><img src="/journal/handmade-texture/002-brick-seamless.png" alt="free seamless brick texture"></a>
Seamless brick texture</p>
</div>

<div class="work-post">
<p class="raw-para"><a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/003-leaves-seamless-hires.png"><img src="/journal/handmade-texture/003-leaves-seamless.png" alt="free seamless fallen leaves texture"></a>
A heap of green leaves</p>
</div>

<div class="work-post">
<p class="raw-para"><a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/004-cartridge-texture-hires.png"><img src="/journal/handmade-texture/004-cartridge-texture.png" alt="free seamless catridge paper texture"></a>
Seamless cartridge paper texture</p>
</div>

<div class="work-post">
<p class="raw-para"><a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/005-mdf-seamless-hires.png"><img src="/journal/handmade-texture/005-mdf-seamless.png" alt="free seamless mdf panel texture"></a>
Seamless MDF board texture</p>
</div>

<div class="work-post">
<p class="raw-para"><a class="thumb" target="_blank" href="https://gitlab.com/raghukamath/Handmade-textures/raw/master/kra/output/hires/006-wall-plaster-hires.png"><img src="/journal/handmade-texture/006-wall-plaster.png" alt="free seamless wall plaster texture"></a>
Seamless wall plaster texture</p>
</div>

</div>

f you find these useful share them with your fellow artists and spread the word, I will also be excited to see how other artists have used these in their artwork. These are free to download but if you like to support me by donating money, you can use my [PayPal link](https://www.paypal.me/raghukamath).
