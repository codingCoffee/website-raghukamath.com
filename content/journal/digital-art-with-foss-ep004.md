+++
title = "Digital art with foss episode 4"
date = "2018-04-29T22:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials"]
thumb = "/img/videos-thumbs/daef-thumb-01.png"
description = "In this video we learn about digital brushes and how brushes work in Krita."
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita"]
slug = "digital-art-with-foss-episode-04"
+++

In this video, I am going to talk about digital brushes. In real life
brushes, pens, knives are the tools used to apply and manipulate
colours on canvas, similarly in digital painting we have a brush tool
that helps to add and manipulate pixels. First, we will see what
digital brushes are made of, which will help us in tweaking and
creating our own brushes in future

## Digital brushes in Krita

{{< video src="/videos/Digital-art-with-krita-ep04-brushes.mp4" poster="video-thumb-ep04.png" type="mp4" >}}

[Download the video](/videos/Digital-art-with-krita-ep04-brushes.mp4)

## Episode Credits

-   Sitar music in the background by
    [Kaiho](https://freesound.org/people/Kaiho) Licensed under Creative
    Commons Attribution 3.0 Unported (CC BY 3.0)
