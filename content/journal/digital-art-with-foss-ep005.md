+++

title = "Digital art with foss episode 5"
date = "2019-03-02T20:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials" ]
thumb = "/img/videos-thumbs/daef-thumb-01.png"
description = "This is the fifth episode in the series on digital art with free and open source software. This episode covers concept of Layers in Digital painting."
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita" ]
slug = "digital-art-with-foss-espisode-5"

+++

This series is about creating digital art in free software. For those
who don't know what free software is please read
[this](https://en.wikipedia.org/wiki/Free_software). This is the fifth
episode in the series on basics of painting in
[krita.](https://krita.org)  


{{< video src="/videos/Digital-art-with-krita-ep05-Layers.webm" poster="video-thumb-ep-05.png" type="webm" >}}

[Download the video](/videos/Digital-art-with-krita-ep05-Layers.webm)

## Episode Notes

In this episode we learn about Layers, their types and way to manage them in Krita. We also see
a basic and simple setup of layers required to create a drawing in Krita.

## Episode Credits

Background Music - [Mridangam Jati](https://freesound.org/people/ajaysm/sounds/194579/) by - Ajaysm  
Licensed under [CC-0](https://creativecommons.org/publicdomain/zero/1.0/).
