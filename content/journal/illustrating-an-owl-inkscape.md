+++

title = "Twilight orchestra in Inkscape"
date = "2017-02-26T12:48:00+05:30"
tags = ["process", "CC-BY-SA", "open source", "tutorials"]
thumb = "/img/owl-thumb.jpg"
description = "A brief look into my process of creating a vector illustration in Inkscape. A music loving owl directing an orchestra in twilight of the moon."
keywords = ["Owl", "Inkscape Illustration", "How to", "vector", "open source", "Twilight Illustration", "Music", "Orchestra", "Music director", "Inkscape"]
slug = "twilight-orchestra-in-inkscape"

+++

I mostly use Krita for my professional and personal illustration work. Every now and then when I require vector illustrations I turn towards [Inkscape](https://inkscape.org).

I have done some professional illustration in Inkscape but they were not so detailed in nature. I am trying to bring more and more vector art into my portfolio. This piece is just to practice and polish my Inkscape skills. I already know the concepts of vector illustration and have used a vector illustration software such as Adobe illustrator and Corel draw. So learning Inkscape is just a matter of learning the new ways or rather Inkscape's way of doing the things which I used to do with illustrator or Corel draw.

I am drawing an owl which is directing an orchestra in the twilight of a moon. I tried to keep the colors to minimum may be around shades of blue and yellow and slight oranges here and there.

At first I scan my initial very rough and ugly sketch of an Owl on a a5 notepad paper. For scanning my artworks I use a software called [Xsane](http://www.xsane.org/xsane-introduction.html). It is very feature complete and simple to use and does what I want without the need of tinkering too much.  


![owl Sketch](/img/owl/owl-sketch.png)

This was just a rough sketch to fine tune it I used [Krita](https://krita.org/en/). I created a New Document of A4 size and simply drag and dropped the sketch into it.  

<blockquote class="quote">
<b>Tip</b> - When you drag and drop an image into Krita (after version 3.0) gives you the option of inserting the image as a file layer. File layers in Krita are just links to the original image files, this comes in handy if you want to update the original image frequently, Any update to the original file will get automatically reflected in the krita document. Neat isn't it :)
</blockquote>
I used my **pack01_basic** brush from my [Brush Pack for Krita V0.2](https://raghukamath.com/journal/brush-pack-for-krita-v20). to create a finer detailed sketch in Krita. The major help here was the mirror symmetry tool in Krita. This sketch was done in almost half of the time it usually takes me to do it, as the other half of the image was completed by the symmetry tool.


![owl sketch](/img/owl/owl-sketch-krita.png)


I then imported this sketch into Inkscape and started to trace the drawing and all the detail using the pen or bézier tool. I traced half of the image and then duplicated this half and flipped it to complete the image. 

You can see the time-lapse of the process in the video below.


{{< video src="/videos/Twilight_Orchestra_Krita_Inkscape.mp4" poster="owl-video-thumb.png"  type="mp4" >}}

The finished vector illustration is below, you can click the image to get the original vector file in svg format. The artwork is under [CC-By-SA 4.0 License.](https://creativecommons.org/licenses/by-sa/4.0/)

<a class="nostyle" href="https://raghukamath.com/img/owl/owl.svg"><img src="/img/owl/owl-finished.png" alt="Ownl Sketch"></a>

