+++
title = "Designing Posters With Free Software"
date = 2019-02-25T14:54:44+05:30
thumb = "/img/fsf-poster-thumb.jpg"
tags = ["cc-by", "tutorials", "process", "open source"]
keywords = ["Free Software", "designing", "krita", "Scribus", "Inkscape", "Poster", "Free Software India"]
description = "A brief account of my process of designing poster for a local Free Software Foundation Event"
draft = false
+++

A few months back I was asked to design some poster for a local Free Software Foundation Event.
Richard M Stallman was [visiting](https://rms-tour.gnu.org.in/) our country, and my friend [Abhas Abhinav](https://abhas.io/) wanted to put up some posters and banners. Due to me being down with flu I couldn’t do the number of banners and posters that I initially intended to do. However, I managed to create two posters for RMS’ talk in Bangalore. This is a short note down of the process that I typically use to create my artwork with F/LOSS (Free / Libre Open Source Software) tools. Although many artists successfully use Free Software to create artworks, I repeatedly encounter many comments in discussion forums claiming that free software is not made for creative work. This post is also my effort in spreading awareness that one can do professional work with the help of F/LOSS tools. I chose this project to document my process because it is not encumbered with NDA as my other ongoing projects and I felt that this topic is closer to Free Software.  


## Sketches

After understanding the initial requirement from Abhas, I sat down to visualize some concepts. I am not that great of a copywriting person, so in order to get some copy material, I started reading the [FSF website](https://www.fsf.org/). Given that I was already late to create anything, I wanted something that would be done in 2 days time, while simultaneously working with other projects. I started sketching some rough layouts. From a lot of 5 layouts, I liked three. I scanned them using Skanlite, these sketches were very rough at that moment and would need proper layouting and design, but they were a good base for me to work.

![skanlite on kde plasma](/img/fsf/skanlite.png)


![initial rough sketches of the posters](/img/fsf/sketch-01.png)
![initial rough sketches of the posters](/img/fsf/sketch-02.png)

I had three concepts -

- One for FSF’s membership program, I had read about taking free software to new frontiers on FSF website, so on those lines, I thought conquering a summit is also similar; free software work is also filled with adventures in my opinion and sometimes the task may seem really like scaling a summit. So I thought showing some mountaineers would resonate well here.

- In the second poster, I wanted to ask people to donate to FSF, so I went with a hand giving a heart, this was not a great thought, and I didn’t feel any excitement in executing this, nevertheless I kept it for backup in case I fell short of time.

- The third sketch was also inspired from FSF website, they have a hashtag for donation program called #thankGNU, so I thought using this as the basis of my design, so repurposing my earlier hand visual, I replaced the heart with a bouquet of flowers, which will have a heart-shaped card saying #thankGNU!. 

I know these are somewhat quick and safe concepts, but given the time I had remaining in my hand, I went ahead with these.

## Making the Illustrations

The process mostly depends on the kind of look that I need in the final image. I choose software and process according to my needs. I may use only one software from start to finish or use various software to accomplish what I need. For this project, [Krita](https://krita.org) and [Scribus](https://www.scribus.net/), with some minimal
use of [Inkscape](https://inkscape.org/).  

I imported these images in Krita and started adding more defined lines and shapes. For the first image, which has some mountaineers climbing, I used [vector layers](https://docs.krita.org/en/user_manual/vector_graphics.html#vector-graphics) in Krita to add basic shapes and then use [Inherit Alpha](https://docs.krita.org/en/tutorials/clipping_masks_and_alpha_inheritance.html), which is similar to what they call clipping masks in Photoshop, to add texture and gradients inside this shape. This helps me to change the underlying base shape (in this case the shape of the mountain in the first poster) anytime during the process. Krita also has a nice feature called reference image tool, you can pin some references around your canvas, this helps a lot and saves many “Alt Tabs”. Once I got the mountain how I wanted according to the layout, I started painting the mountaineers and added more details on the ice etc. I like grungy brushes and brushes that have a texture akin to chalks and sponge, Krita has a wide range of brushes as well as brush engine, which makes replicating traditional medium easier. After about 3 ½ hrs of painting, this image was ready for further processing.  

In the second image I wanted to have a feel of old-style book illustration, So I created the illustration with inked lines, somewhat similar to what we have in textbooks or novels. Inking in Krita is really a time saver since it has stabilizer options, your wavy hand drawn lines will be smoothed and crisp with it. I added a textured background and some minimal colors beneath the lines, It took me about 3 hrs to do this illustration as well.  

![Finished illustration](/img/fsf/poster-illo-01.jpg)


![Finished Illustration number 2](/img/fsf/poster-illo-02.jpg)

## Layout and Typography

Once my illustrations were ready, It was time to move on to the next part. Adding the text and other things to the layout. For this I chose Scribus. Both Scribus and Krita have CMYK support. You can soft proof your artwork make changes according to the color profile that you get from the printer in both applications. But I mostly do my work in RGB and then if required I convert them to CMYK, most printers will do the color conversion nowadays so it is not required, however in case it is needed, we can work in CMYK with Free Software tools.  

I use only Opensource Fonts for my design work, and use any closed font only if the client has licensed them for use. One good way to browse for suitable fonts is google font repository. I have the entire repository cloned and I can search for fonts via their [website](https://fonts.google.com/). Occasionally I also browse fonts on [Font Library](https://fontlibrary.org/) they also have a nice collection. I decided to use Montserrat by Julieta Ulanovsky for the posters. Placing text was very quick in Scribus, once you make a style you can apply the same style to any number of paragraph or title. This helped in quickly placing text in both designs as I didn’t have to recreate the text properties.  

![Illustrations in Scribus](/img/fsf/poster-in-scribus.png)

I keep two layers in Scribus, one for the illustrations, which are just linked to the original files so that if I change the illustrations it will update in Scribus. The other layer atop of this is a text layer. I used Inkscape to generate a QR code which points to an appropriate URL from FSF’s website. To generate a QR code go to ***Extensions > Render > Barcode > QR Code*** in Inkscape menu. The logos are also vector which Scribus supports, you can directly paste things from Inkscape into Scribus. In a way, this helps in designing CMYK based vector graphics.

![Final Posters](/img/fsf/final-01.png)


![Final Poster number 2](/img/fsf/final-02.png)

With the Designs ready, I exported them to layered PDF and sent to them to Abhas for feedback. He had some feedback to include FSF India’s logo, which then I added and sent the pdf back again. From here on Abhas took over the printing part of the process. He has a local printer in Bangalore, he got these posters printed in A2 size. he was kind enough to send me some pictures of these. The prints came out well, considering I didn’t even convert them to CMYK, nor did I do any color corrections or soft proofing that I usually do when I get the color profile from my printer. My opinion is that 100% accurate CMYK printing is just a myth, there are too many factors to consider if you really want perfect color reproduction, I often leave this job to the printer, who knows his printer well and does the conversion himself.

![Printer Posters in action](/img/fsf/posters-in-action-01.jpg)


![Poster number two in action](/img/fsf/posters-in-action-02.jpg)

## Source Files For All

When we discussed the requirement for these posters Abhas had told me to release the artwork under CC-BY license so that others can re-use modify and share these. I am really glad he mentioned it. So for anyone who wants to poke at the files I have uploaded them to my drive -> [here](https://box.raghukamath.com/cloud/index.php/s/97KPnTBP4QL4iCx). If you have any improvements to make please go ahead and do remember to share it with everybody.
