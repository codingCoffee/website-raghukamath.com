+++

title = "Digital art with foss episode 2"
date = "2018-04-22T21:48:00+05:30"
tags = ["Krita", "CC-BY", "open source", "tutorials"]
thumb = "/img/videos-thumbs/daef-thumb-01.png"
description = "This Episode talks about customising and making your workspace in krita. We also explaore the dockers in Krita."
keywords = ["krita", "Basics", "Tutorial", "digital art with foss", "interface basics", "UI", "krita"]
slug = "digital-art-with-foss-episode-2"

+++

This series is about creating digital art in free software. For those
who don't know what free software is please read
[this](https://en.wikipedia.org/wiki/Free_software).

## Workspace and Dockers

{{< video src="/videos/Digital-art-with-krita-ep02.mp4" poster="video-thumb-ep02.png"  type="mp4" >}}

[Download the video](/videos/Digital-art-with-krita-ep02.mp4)

## Episode Notes

Krita's flexible workspace is really easy to customise according to our
needs. In this video we se how to customise and create our own
workspace. We also learn about dockers in Krita.

## Episode Credits

-   Swaramandal by
    [luckylittleraven](https://freesound.org/people/luckylittleraven/)
    Licensed under CC0.
-   Sitar music in the background by
    [Kaiho](https://freesound.org/people/Kaiho) Licensed under Creative
    Commons Attribution 3.0 Unported (CC BY 3.0)
