+++
title = "Digital Landscape paintings"

date = "2017-02-12T11:30:00+05:30"
tags = ["sketches", "process"]
thumb = "/img/landscape-thumb.jpg"
description = "Few paintings from a series of digital landscape studies done to imrpove my understanding of color and value in nature."
keywords = ["digital landscape", "plein-air", "krita", "floss painting", "timelapse youtube", "timelapse youtube", "krita tutorial"]
slug = "digital-landscape-paintings"
+++

I have been following the awesome [James
Gurney](http://www.jamesgurney.com/) for quiet some time now. His book
[Color and Light- A Guide for the Realist
Painter](http://www.amazon.in/Color-Light-Realist-Painter-Gurney/dp/0740797719/ref=sr_1_2?ie=UTF8&qid=1486880703&sr=8-2&keywords=color+and+light)
is an excellent resource for painters. Inspired by both his book and his
youtube videos, I wanted to spend some time studying landscape painting
and improving my understanding of colors and values present in nature.

Here is the first set of such paintings

![landscape painting in krita](/img/landscape/landscape-001.jpg)

![landscape painting in krita](/img/landscape/landscape-002.jpg)

I also recorded the process of some painting

{{< video src="/videos/Before-Noon-Made-in-krita.mp4" poster="before-noon-video-thumb.png"  type="mp4" >}}

![landscape painting in krita](/img/landscape/landscape-003.jpg)

I also tried a vector only landscape painting. This piece was done using
inkscape only. The gradient and filters in inkscape helped me achive the
desired look. I want to do some more of vector only paintings in future.
You can download the source file for this painting from
[here](/downloads/evening-hills.svg).

![landscape painting in krita](/img/landscape/landscape-004.jpg)

![landscape painting in krita](/img/landscape/landscape-005.jpg)

{{< video src="/videos/Lonely_Ranch_speedpaint_in_krita.mp4" poster="lonely-ranch-video-thumb.png"  type="mp4" >}}
