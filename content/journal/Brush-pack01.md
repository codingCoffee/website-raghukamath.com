+++
title = "Brush Pack for Krita"
date = "2016-03-10T22:17:00+05:30"
tags = ["open source", "brush-packs", "resources"]
thumb = "/img/brushpack-thumb.jpg"
description = "Some of my brushes that I use while painting with Krita. free to download, use and modify."
keywords = "Krita, brush bundle, brush pack, krita, linux, krita 3.0"
slug = "brush-pack-for-krita" 
+++

In my previous [post](http://raghukamath.com/journal/switch-to-freedom), I mentioned that I switched from Photoshop to [Krita](https://krita.org/features/highlights/). The workflow is much better and, I really enjoy working in this application. The brush engine is very robust and one case lose almost hours playing and experimenting with it. There are in total 16 brush engines in Krita, it can be somewhat overwhelming for a new user at first but you will definitely be surprised the level of customisation it gives.  

There is a lot of room for experiments and happy accidents. One thing to note is that in Krita the brush dap is called brush tip and the preset themselves are called paint top presets. I like to keep my brushes to a minimum number, I create brushes on the fly and delete unused brushes. I am sharing the most basic brushes which I use almost daily. Most of them belong to [Pixel brush engine](https://docs.krita.org/Pixel) and two of them are [Bristle brush engines](https://docs.krita.org/Bristle).  


![brush-preview](/img/brush-preview-02.png)  


**[DOWNLOAD](https://github.com/raghukamath/krita-brush-presets/archive/master.zip)**

You can download the brush pack from [here](https://github.com/raghukamath/krita-brush-presets/archive/master.zip).  

To install, just unzip the contents of the zip file, then in Krita got to settings > Manage resources, then click on open resource folder button. A folder will be opened in the file explorer then copy the contents inside the unzipped folder namely folders named - brushes and paintoppresets, then paste and merge the contents to the folders inside the resources folder. Then restart Krita, the brushes are ready to use, just search for pack01 in the brush filter. Go [here](https://docs.krita.org/Loading_and_Saving_Brushes) for more clear instructions.  

I have added these brushes to a github repository [here](https://github.com/raghukamath/krita-brush-presets), so you can fork it or add suggestions there.  

I am sharing these with [CC0](https://creativecommons.org/publicdomain/zero/1.0/) licence, that means you can do whatever you want with them.  

I am working in krita 3.0 currently, so I can't guarantee that these work in earlier version, although they may work in 2.9. Any improvement, suggestion or addition to the brush presets are welcome.
