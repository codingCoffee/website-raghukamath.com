+++
title = "Is working with CMYK worth it?"
date = 2019-11-15T21:00:44+05:30
thumb = "/img/CMYK-thumb-600.png"
tags = ["process", "open source"]
keywords = ["Free Software", "designing", "krita", "Scribus", "Inkscape", "CMYK"]
description = "My opinion and workflow about working in CMYK from start to finish using Free and open source creative tools "
draft = false
+++

Over the years it has been an unspoken rule to choose CMYK right at the start when creating new documents for artworks that are going to be printed. Many old-timers may remember working in CMYK from start to finish carefully choosing colors and keeping an eye on the out of gamut warnings while color picking. Despite the effort of these people it would often lead to unsatisfactory results.  

In the Free Software side of the world, many users demand the CMYK support in apps and tell the developers that it is the single most reason they don't want to switch. So the question here is - Is working in CMYK from start to finish worth it? Are Free Software tools capable of CMYK workflow?

I would say no for choosing CMYK as default workspace for working and also that Free software tools nowadays are more than capable to handle CMYK in your workflow. 

You lose many things just to chase down an unpredictable result. The first and foremost is that you lose a great number of color choices. You lose the ability to add filters, you get only a few blend modes to choose from and above all you limit your artwork to be in a color profile which is slowly losing its foothold. Nowadays Many people consume media on screen rather than paper. So it is very silly to target only one section of the audience. 

If you work in RGB you are not restricted with the number of colors, filters or blend modes, etc. You can also convert your artwork later for print consumption.

Now let us see how CMYK is unpredictable. Getting your artwork printed exactly as you see on the screen is an uphill battle. You need to have color corrected and calibrated monitor, You have to know exactly on which paper your artwork is going to be printed. And the most important of all, you would need to have the color profile of the printer that you are going to use. Each printer has a different color profile, then the printer needs to be calibrated too. 

If you haven't ever asked for the color profile from your print shop then you have been doing it wrong all the time. From what I have seen many artists even those working in big agencies and companies just use the default SWOP profile and move on with their work. Their work gets printed in a sheet fed printer and they don't even know that they are using a profile meant for the web press. And even if they use the SWOP profile the guys at the print shop do the conversion to the final color profile anyway. So in the end, I find the effort of these people wasted.

## What is my workflow?

I do all my work in RGB and later based on the requirement I convert my artworks to CMYK. When I am working with a client, I ask them to provide a color profile from the print shop. Most of the time my clients get the conversions done at the printer and since the artwork will be used in various mediums, they don't ask the artwork in CMYK from me. And besides most print shops are now advanced enough to accept RGB images. The color profile I get from the print shop is created after taking into consideration various factors such as the performance of the printer, the type of paper which is going to be used, type of printing method, quality of inks, etc. The people at the print shop know about the printer better than I do so the color profile that they provide is going to be the best. 

Then I import this color profile in [Krita](https://krita.org) or [Scribus](https://www.scribus.net/) and then I check if my colors are beyond the printer's color gamut. This is called soft proofing. Contrary to the naysayers, both Krita and Scribus (Free Software tools) have this capability. So even if I do my artwork in [GIMP](https://www.gimp.org/) or [Inkscape](https://inkscape.org/).  I can always import my artwork in Krita or Scribus to proof the colors. Krita and Scribus will warn you about any color that is out of range and you can correct those colors. Since my monitor is calibrated I don't have to worry about the truthfulness of what I see on my screen. When I am satisfied I save a copy of the artwork which is flattened and then converted to the target color profile with correct rendering intent. This file is sent for Hard proofing i.e. getting an actual sample print. This clears any remaining discrepancies that may come in later due to the factors such as quality and texture of paper type of inks etc. 

I have been using this workflow for many years and I have seldom got any muddy prints. So in my opinion working in only CMYK is not worth it, you should work in RGB and then convert it to printers CMYK  profile for the final print. Besides, you can choose to work with some other print shop in the future which will have a different color profile, so it doesn't make sense to keep your open files in one obscure CMYK color profile.

What is your opinion and what is your workflow? I would be glad to hear them.

This article was originally posted by me on [Krita-artists.org](https://krita-artists.org/t/is-working-in-cmyk-worth-it/129).
