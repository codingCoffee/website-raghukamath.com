+++

title = "Brush pack for Krita V2.0"
date = "2016-11-12T00:30:00+05:30"
tags = ["open source", "brush-packs", "resources"]
thumb = "/img/brushpackv2-thumb.jpg"
description = "Some of my brushes that I use while painting with Krita. free to download, use and modify."
keywords = ["Krita", "brush bundle", "brush pack", "krita", "linux", "krita 3"]
slug = "brush-pack-for-krita-v20"

+++

In my previous
[post](http://raghukamath.com/journal/brush-pack-for-krita), I had
shared some of the brushes that I use while painting in
[Krita](https://krita.org/features/highlights/). This is the second
release of that brush pack. In this release I have added some more
presets to the pack and also tweaked some of the brushes to utilise
newly added parameter in Krita such as
[ratio](https://docs.krita.org/Parameters#Ratio). I have also reduced
the brush tip images used in the pack and utilised the default auto
tips. An example config can be seen in the image below.

![Auto-tip](/img/brushpack-auto-tip.jpg)

I also refreshed the brush icons a bit in this release.

![brush-preview](/img/brushpack-v2-preview.png)

**[DOWNLOAD](https://gitlab.com/raghukamath/krita-brush-presets/archive/v2.1.zip)**

You can download the brush pack from
[here](https://gitlab.com/raghukamath/krita-brush-presets/-/archive/v2.1/krita-brush-presets-v2.1.zip)
.

To install, just unzip the contents of the zip file, then in Krita got
to settings \> Manage resources, then click on open resource folder
button. A folder will be opened in the file explorer then copy the
contents inside the unzipped folder namely folders named - brushes and
paintoppresets, then paste and merge the contents to the folders inside
the resources folder. Then restart Krita, the brushes are ready to use,
just search for pack01 in the brush filter. Go [here](https://docs.krita.org/en/reference_manual/resource_management.html#resource-management) for more clear instructions.

This time I have also created a bundle file for this pack, you can
download the bundle file from [this
repo](https://gitlab.com/raghukamath/krita-brush-presets/blob/master/bundles/Raghukamath-Brush-Pack.bundle).

I have added these brushes to a gitlab repository
[here](https://gitlab.com/raghukamath/krita-brush-presets), so you can
fork it or add suggestions there.

I am sharing these with
[CC0](https://creativecommons.org/publicdomain/zero/1.0/) licence, that
means you can do whatever you want with them.
