+++
title = "Krita Sprint 2018"
date = "2018-05-23T12:48:00+05:30"
tags = ["open source", "sprint"]
thumb = "/img/krita-sprint-18-thumb.jpg"
description = "I took part in the 2018 Krita sprint, it was an awesome experience meeting everyone I know from the #krita IRC."
keywords = ["Krita sprint 2018", "Krita", "Sprint", "FOSS", "ditital art software", "manual", "krita", "develeopment", "free software"]
+++

I had an awesome opportunity to be a part of this years
[Krita](https://krita.org/en/) sprint in The Netherlands. Once in a
while artists and developers who are working in Krita meet to discuss
bugs, improvements and roadmap for Krita. The Sprint was organised by
the Krita Foundation and KDE e.V.

I got to meet all the people that I know only by IRC nicknames. Boud and
Irina were excellent hosts and made sure that everyone was feeling
comfortable. I started working with [Wolthera](http://wolthera.info/)
and [Timothée](http://timotheegiet.com), porting the remaining pages of
the manual in the tutorial section to Sphinx. We are porting the manual
from MediaWiki to Sphinx to make sure you can download the manual in pdf
form for offline use.

I met Valarii Malov who is doing an awesome job maintaining the graphics
tablet KCM in KDE. . We had some talks and testing of the KCM and
believe me the KCM is awesome in its current state, it exposes various
setting which was only possible via xsetwacom. This work will help many
artists who will use KDE as their distro for daily work.

A big thanks to Timothée for solving my color calibration problem, now
my laptop is not blueish and has proper color. I tried to do some
portraits and sketches during the sprint. I couldn't complete all of
them but below is a rough portrait of our hosts Boud and Irina.

![portrait of Boud - lead developer of Krita](/img/krita-sprint/boud.jpg)

![portrait of Irina](/img/krita-sprint/irina.jpg)

On the second day of sprint we had discussion about the fund raising for
Krita, we will be doing a fund raiser for Krita in September, it will be
sort of Krita celebration month for us :) We also had a good and
detailed discussion about the overhaul of resource manager in Krita, we
have given a lot of thought and talked about all the scenarios regarding
the resource management, I hope the future version of Krita will have
awesome resource management.

Boud gave us his hardware and graphics tablet to try and test, We also
had fun booting up ubuntu on his Wacom mobile studio. Apart from some
hiccup while using the touch keyboard everything worked out of the box.

We also discussed the pending things that need to be finished and
artists were asked to prioritise the list according to their preference.

While the artists painted and reported bugs, developers were busy fixing
bugs and planning new features and improvement. Typpi showed us a very
nice implementation of hue and saturation, all of the artists were very
excited to see it

I had a very great pleasure to meet Steven Schoorens, Wolthera, Jouni
Pentikäinen, Valerii Malov, Alvin Wong, Dmitry Kazakov, Boudewijn &
Irina. I also met [David](http://www.davidrevoy.com/) and Timothée once
again. Boud showed us around the city of Deventer, which was beautiful,
fresh and clean. We had some good walks around the city everyday.

![A group photo that was taken during one of our walks around the city
of Deventer](/img/krita-sprint/group-photo.jpg)

It was really a great experience travelling to Deventer and meeting all
the people who make Krita an awesome application for artists, you can
read the full report of the sprint on the official Krita [Blog
Post](https://krita.org/en/item/krita-2018-sprint-report/).
