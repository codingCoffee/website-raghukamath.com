+++

title = "Static is good"
date = "2015-12-28T15:02:00+05:30"
tags = ["open source"]
thumb = "/img/static-thumb.jpg"
description = "As a weekend project I switched from wordpress driven website to static website"
keywords = ["jekyll", "pelican", "ruby", "python", "jinja", "static site generators", "pelican illustrator portfolio"]
slug = "static-is-good"

+++

My old website was built using Wordpress with a custom theme. I had used
this setup for almost 2 years. Recently when my hosting plan was up for
renewal I decided to move to a new hosting provider, and while I was
migrating my website I thought on renovating the website, just to revise
and brush up the little bit knowledge I have about web development.
Initially I didn't mean to switch away from Wordpress, but as I browsed
and looked at some other peoples blog and portfolio I stumbled upon a
ruby powered static site generator called
[jekyll](https://jekyllrb.com/).

I like the idea of using pre-rendered website rather than a database and
cms rendered website. Using static website also meant I would get a fast
and snappy website and users wont be required to wait for the site to
fetch content from the database.

I also liked the fact the maintenance required is considerably lower
than Wordpress website, where you have to update the engine whenever
newer Wordpress version gets released. The website will also be portable
I can carry it on a pen-drive or just copy the folder while migrating to
a new hosting provider, no exporting and importing database contents
etc. Believe me it is really less hassle.

You write the blog post in markdown with your favourite text editor (in
my case Kate and nano), then regenerate the website and upload the
content via Ftp, scp or rsync. Some times I just copy paste the file by
accessing the webserver from my file manager - Dolphin. Some jekyll
users use free github pages for their websites.

As the content of my website is not updated on a daily basis I thought
using a cms is a bit of overkill, hence I chose jekyll here, there are
other static site generator like Hugo, Roots and Middleman, I will
eventually try these too but for now I am comfortable with jekyll

While I am not a professional developer and I am just starting to
discover things, with the help from my sister I have managed to build
this website with jekyll, I am sharing the pages and code on my [gitlab	
repository](https://gitlab.com/raghukamath/website-raghukamath.com) so
that anybody looking for a simple portfolio website can use it. The code
is not that great but I think it will be usefull for noobish people like
me who want to setup a portfolio website with minimal effort and focus
more on drawing and painting :). The code is licenced under MIT licence
while the original content like images, articles photographs are under
[CC BY-NC-ND 4.0
license](http://creativecommons.org/licenses/by-nc-nd/4.0/).

I will be trying to improve the code gradually when I learn more, I am
also welcome any tips and corrections from anyone. Feel free to fork it
and use it to create your own portfolio website.

## Update - Jekyll to Pelican

After dabbling a bit in jekyll I now switched to
[Pelican](http://blog.getpelican.com/), This was a natural choice as I
found that pelican is python based and since I am learning python as
hobby I thought it would be fun. Although jekyll is absolutely good ,
pelican seems good for me now. It was fairly easy to port the theme and
templates to jinja from liquid. I am also learning restructured text to
write the post. But I must agree markdown is simple and easy :)

## Update 2 - Pelican to Hugo

I now use hugo for generating this site, the theme hasn't changed much
I also migrated the .rst files to .md file with the help of pandoc and vims search and replace capabilities
