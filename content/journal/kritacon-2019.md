+++
title = "KritaCon 2019"
date = "2019-08-13T15:00:00+05:30"
tags = ["open source", "sprint"]
thumb = "/img/krita-sprint-19-thumb.jpg"
description = "Once again I took part in the Krita sprint (convention). It was fun to meet and talk about Krita with contributors from all over the world"
keywords = ["Krita sprint 2019", "Krita", "Sprint", "FOSS", "digital art software", "manual", "krita", "development", "free software"]
+++

<figure class="mh0 mt-md">
  <img src="/img/sprint-19/sprint_group.jpeg" alt="The Krita Team">
  <figcaption class="italic text-center">The Krita team - Photo courtesy David Revoy/Krita Foundation - CC-BY</figcaption>
</figure>

Once again this year, I participated in Krita sprint held at an awesome historical town of [Deventer](https://en.wikipedia.org/wiki/Deventer). Thanks to [KDEeV](https://ev.kde.org/) and [Krita Foundation](https://krita.org/en/about/krita-foundation/) for sponsoring my stay and travel.
I got to meet some old friends again and had a chance to be acquainted with new members of the Krita community. I also feel happy to say that this is so far the biggest Krita sprint in terms of the number of attendees. It shows that our community is growing big rapidly.


## 5 August

I reached Deventer a bit late in the afternoon on Monday 5th August. I was greeted by some new faces like Emmett, Eoin, Tusooa (who gave an awesome key-chain to everyone) Mariya, Alberto, Noemi and some familiar ones like [Wolthera](https://wolthera.info/), Stefan, Scott, Tyyppi, Dmitry, and of course the wonderful hosts Boud and Irina.

We had some chat and I took some rest for the day.

## 6 August

![view of deventer from the roof of our hotel](/img/sprint-19/deventer-morning.jpg)

On Tuesday we welcomed some more members. I met with [Hellozee](https://www.hellozee.dev/krita_sprint_2019/), Rebecca, Agata, Ivan and Sara for the first time. I also felt sad that I didn't get a chance to meet Sharaf Zaman, who has done a wonderful job of porting Krita to Android. Since many people were still on their way and we had postponed the meeting to Thursday, We discussed some issues and kick-started the sprint. I got a chance to test the [HDR](https://en.wikipedia.org/wiki/High-dynamic-range_imaging) rig that Boud had set up to test in the cellar. I had some mixed feelings about this, although the monitor had a great range of colors, the brightness was too much for me to handle. I look forward to the day when these monitors become common in the industry and maybe the day when Linux supports HDR monitors :D.

I also got to try the android port of Krita, and it was fascinating to see the full-scale desktop application decently working on the Samsung tablet, the menus, tools and all the functionality from the desktop version was there in the app already, the pressure sensitivity was working great. Scott and I had some talk and observations about UI of the android port. Later Wolthera showed us her workspace arrangement for the tablet and I think that should be made the default for android builds :) it was clever. Now I know what gadget to buy next. A tablet with Krita on it would be a great companion for the artists who want to do some Plein air paintings. 

Later in the evening we had dinner and had a wonderful walk around the town. I did some sketches while waiting for the delicious dinner to be served.

![Sketch during dinner Krita sprint](/img/sprint-19/sketch-03.jpeg)


![Sketch during dinner Krita sprint](/img/sprint-19/sketch-02.jpeg)



![Sketch during dinner Krita sprint](/img/sprint-19/sketch-01.jpeg)


## 7 August

Boud had arranged for a museum trip for everybody so that we get to know each other a bit more. We had brought some art tools to paint and have fun too. I did some watercolor studies and some sketches while touring the [Open Lucht Museum](https://openluchtmuseum.nl/en). The museum had some wonderful collection of the real world-sized windmill, some of which were even working, types of houses, clothing, living conditions of people throughout dutch history. The ambiance was calm and apt for doing some Plein airs. Almost everyone had drawn at least one windmill.


![watercolor studies](/img/sprint-19/bridge.png)



![watercolor studies](/img/sprint-19/windmill.png)

We had dinner and went to rest for the day, my roommates were Tyyppi, Stefan, Hellozee, David and Dmitry. I had fun and loud laughing spree with David and Dmitry by up a bit late in the night and discussing necessary evils :D, I probably won't forget this subject for the rest of my life, thanks both of you :D

## 8 August

This was the meeting day. We had a long extensive meeting which spanned for the entire day, you can read more about the details of the things discussed in this meeting on the [Krita Blog](https://krita.org/en/item/krita-2019-sprint/). Although the meeting was exhausting for us it had some really important and useful discussions.
After the meeting, we went to dinner at a local restaurant.  

After coming back from dinner Boud was generous enough to let us use his awesome [water mixable oil paints](https://www.royaltalens.com/products/water-mixable-oil-colours/), those felt like gouache but were really oil paints without the smell of the linseed and spirit. I painted a windmill again, but this time from memory.

![A painting with water mixable oils](/img/sprint-19/oils.jpg)

## 9 August

We discussed some new features and some improvements on Friday. David left early. I got to test magnetic lasso implemented by Hellozee and also got to test it with comparison to photoshop. I managed to crash photoshop too :). I and Dmitry tested the move tool and general speed of photoshop, we concluded that we are on par and if not better than photoshop with regards to speed of drawing strokes on canvas and moving object. we managed to crash photoshop a lot.  

After trying photoshop after 4 years, I am of an impression that it has degraded in quality and usability compared to earlier versions. Maybe because Krita has spoiled me with its so many cool workflow optimizations.


<figure class="mh0">
  <img src="/img/sprint-19/testing_ps.jpg" alt="stress testing PS">
  <figcaption class="italic text-center">Stress testing Photoshop's Magnetic Lasso tool - Photo courtesy Kuntal Majumder</figcaption>
</figure>

We had some discussion about snapshots, compositions docker and session management feature in Krita. Dmitry already showed some improvements he made on the move tool performance. Scott was leaving early in the morning the next day so we said goodbye to him, it was great to meet him face to face.

## 10 August

The sprint had already ended and people were starting to leave one by one. It was a bit sad to say goodbye to everyone. I hope I get a chance to meet everyone again and also to see some more new faces next time in the sprint.
Thanks to Boud, Irina, and KDEeV for hosting us.


<figure class="mh0">
  <img src="/img/sprint-19/group.jpg" alt="Krita Team">
  <figcaption class="italic text-center">Krita Team - clicked by Krzyś - Photo courtesy Krita Foundation</figcaption>
</figure>
