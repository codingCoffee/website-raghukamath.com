+++
title = "Mermaid illustration process"
date = "2015-12-15T12:19:00+05:30"
tags = ["process"]
thumb = "/img/process-mermaid-thumb.jpg"
description = "This piece was done in between work, I have recorded most of the process, you can watch the time-lapse video below."
keywords = ["mermaid", "illustration", "process", "tutorial", "speed painting", "krita", "linux artists"]
slug = "mermaid-illustration-process"

+++

An illustration done in free time using Krita. This piece was done in
between work, I have recorded most of the process, you can watch the
time-lapse video below. The fish scales were fun to paint , it was easy
to paint them in wrap around mode of [Krita](https://krita.org/) a real
time saver

{{< video src="/videos/Illustrating_a_mermaid_in_Krita.mp4" poster="mermaid-video-thumb.png"  type="mp4" >}}

### Credit
Music : Jellyfish In Space - Kevin MacLeod

**The completed illustration**

![Mermaid Illustrations](/img/mermaid-full.jpg)
