+++
title = "Two hour speed painting"
date = "2015-11-30T23:27:00+05:30"
thumb = "/img/speed-painting-thumb.jpg"
tags = ["process"]
description = "2hr speed-painting in krita. I tried to finish this painting in 2 hr, I might have taken 5-10 minutes extra :) "
keywords = "speed painting, krita, linux, floss painting"

+++

2hr speed-painting in krita. I tried to finish this painting in 2 hr, I
might have taken 5-10 minutes extra :)

{{< video src="/videos/rise-timelapse.mp4" poster="rise-video-thumb.png"  type="mp4" >}}

### Credits
Music - Living Voyage by Kevin MacLeod is licensed under a Attribution 3.0 International License.

The finished painting

![speedpainting krita](/img/speed-painting-full.jpg)
