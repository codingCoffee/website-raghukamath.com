+++
title = "The hunt - timelapse painting"
date = "2016-11-15T23:19:00+05:30"
tags = ["process"]
thumb = "/img/process-hunt-thumb.jpg"
description = "I recorded my screen during the last painting session here is the timelapse video of the painting."
keywords = ["hunt", "illustration", "process", "tutorial", "speed painting", "krita", "linux artists"]
slug = "the-hunt-timelapse-painting"
+++

I recorded my screen during the last painting session. It took me 5hrs
to do this piece the video is speeded up so that it can fit in 5
minutes. I plan to record more such painting it helps me to find weak
points in my work flow :)

{{< video src="/videos/The_Hunt_Timelapse_painting_in_Krita.mp4" poster="hunt-thumb-video.png"  type="mp4" >}}

