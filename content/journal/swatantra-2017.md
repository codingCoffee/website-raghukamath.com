+++
title = "Swatantra 2017"
date = "2017-12-23T12:48:00+05:30"
tags = ["open source"]
thumb = "/img/swatantra-17-thumb.jpg"
description = "My experience of being a part of an awesome free software conference by icfoss."
keywords = "Swatantra 2017, icfoss, raghukamath, kerala, trivandrum, open source, freedom, foss, free software, swatantra"
+++

![Artwork by ICFOSS team](/img/swatantra-17/header-swatantra-17.jpg)

[ICFOSS](https://en.wikipedia.org/wiki/ICFOSS) is an organisation set up
by Govt. of Kerala, with a goal to spread free software knowledge by
connecting and collaborating with international free software
communities and organisations. I was invited to be a part of their
triennial conference called
[Swatantra](https://en.wikipedia.org/wiki/Swatantra_2014) 2017. There
were close to 22 speakers from all over the world and this time there
was a section on art in open source. This is the first time that a free
software conference in India had a section focused on art.

The venue was a nice heritage hotel in Trivandrum, Kerala. Trivandrum is
a fantastic city, it is calmer and laid back when compared to Mumbai, it
has some Art-deco styled buildings which are often fused with
traditional Kerala style architecture. Spent a wonderful time there with
[David Revoy](http://www.davidrevoy.com/) , [Timothée
Giet](http://timotheegiet.com/blog/), [Praveen](http://www.j4v4m4n.in/),
Sumantro, Yogesh, Aruna, other free software enthusiasts from
[fsci](http://fsci.org.in/). It was wonderful to meet Arun, Abhas and
also Nishant who records music on Linux!

At the conference we were greeted by goodies, they gave us an awesome
bag, USB and other items. The conference started with a keynote speech
from [Karen
Sandler](https://sfconservancy.org/news/2017/dec/22/karen-swatantra/),
followed by amazing sessions by David Revoy who explained the making of
[Pepper & Carrot](https://www.peppercarrot.com/) ,
[Sumantro](https://fedoraproject.org/wiki/User:Sumantrom) explained how
to contribute to Fedora, [Aruna](https://twitter.com/arunasank) spoke
about how foss empowered her. Timothée showed us how to animate in
Synfig and the journey of Gcompris until now. There were many speakers
and parallel sessions and I was confused in choosing which ones to
attend:).

![Photo courtesy David Revoy](/img/swatantra-17/raghukamath-talk.jpg)

I had a [talk](https://raghukamath.com/talks/swatantra-17/) about how
libre-software helps me in creating commercial art, you can take a look
at the slides and the talk
[here](https://raghukamath.com/talks/swatantra-17/), although there was
a technical glitch during the talk, the audience was very patient and I
had wonderful questions from them at the end of the talk. Some of them
also asked me questions after the talk, I would be glad to help them
more and spread more awareness about using free software to create art.
I must thank Timothée for giving me his laptop to do my presentation
without which I couldn't have shown the demo of the software. This talk
was a unique experience for me and I learnt that no matter how ready you
are there can be anything that can go wrong during a live demo, so in
future I will make it a point to test everything on the venue before a
day of the actual talk, this really felt like an adventure. I talked to
some enthusiastic students from the college of fine art Kerala and also
the design team of ICFOSS, I was glad to answer their questions and
share my thoughts on working with open source software for design and
illustrations.

![Awesome bus with disco lights and
music](/img/swatantra-17/swatantra-bus.jpg)

There were dinners, musical night and gathering arranged for us, we also
had a tour to nearest Kovalam Beach organised by ICFOSS, it gave nice
opportunity to talk and get myself acquainted with other speakers. On
the first day, during one of the dinners prior to the event, it was
really nice talking about India and it's diversity with Todd Weaver,
CEO of [Purism](https://puri.sm/) a freedom respecting laptop
manufacturing company.

The second day we had a plenary talk from Todd about ethical computing
and ethical devices. There were panel discussions about the current
state of free software in society and how we should improve it. After
the event I spent a day with awesome David Revoy and his wife Alicja, we
had some fun visiting local zoo, shopping in the local market and had a
great time visiting the College of Fine Arts Kerala. My phone battery
died and I don't have any pictures :( This picture is from David and
Alicja.

![A visit to College of Fine Art Kerala, Photo courtesy Alicja & David
Revoy.](/img/swatantra-17/fine-art-visit.jpg)

This was an amazing experience and I really enjoyed the week and I must
really thank Arun, Praveen, Siva & Hasiya from ICFOSS team for making
this happen and giving me an opportunity to speak, they did an excellent
job managing this entire event, and it was perfectly organised. I hope
to be a part of future events too.
