+++
title = "Tapri"
date =  "2015-11-30T16:42:00+05:30"
workthumb = "/img/tapri-thumb-300x300.jpg"
thumb = "/img/tapri-thumb-600.jpg"
tags = ["graphic", "personal"]
description = "A small series of illustrations depicting people and things that I saw at Tapri (Indian roadside Tea-stalls) at various places."
keywords = ["Indian Tea Stalls", "Travel", "Tea India", "India", "Tea maker", "Tea seller"]
+++

## An Indian Tea Stall

A small series of illustrations depicting people and things that I saw
at Tapri (Indian roadside Tea-stalls) at various places. Tapri is a
place where we relax, gossip, hangout, chalk out business plans,
propose, and do various other things. I think everybody has some stories
related to this place. If you have one please share, I will be glad to
listen.

Now you can buy the prints from my society6 page
[here](https://society6.com/product/tapri-indian-tea-stall-5fc_print#1=1).

![Tapri - Indian Tea Stall](/img/tapri/01.jpg)

![Tapri - Indian Tea Stall](/img/tapri/02.jpg)

![Tapri - Indian Tea Stall](/img/tapri/03.jpg)

![Tapri - Indian Tea Stall](/img/tapri/04.jpg)

![Tapri - Indian Tea Stall](/img/tapri/05.jpg)

![Tapri - Indian Tea Stall](/img/tapri/06.jpg)

![Tapri - Indian Tea Stall](/img/tapri/07.jpg)
