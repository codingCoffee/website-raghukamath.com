+++
title = "Pidilite Elephant Adhesive"
date = 2016-07-21T21:00:34+05:30
client = "pidilite"
thumb = "/works/pidilite-elephant/elephant-thumb-600.png"
workthumb = "/works/pidilite-elephant/elephant-thumb-300.png"
tags = ["commission", "realistic"]
keywords = ["pidilite", "elephant brand", "elephant illustration", "pidilite elephant glue"]
description = "A commission Illustration done for Pidilite Industries India for their Elephant Brand Adhesives"
+++

Pidilite Industries commissioned me to create elephant Illustration for their Elephant brand adhesive for furniture and marbles.

![Elephant standing on a sturdy chair - pidilite](elephant-01.png)  

![Elephant climbing on marble stairs - pidilite](elephant-02.png)  

![Some details of the elephant illustration - pidilite](elephant-03.png)  


Artwork is under copyright of Pidilite Industries.
