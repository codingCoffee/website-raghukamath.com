+++
title = "Mascot for Pulvitec Brazil"
date = "2017-03-03T09:18:00+05:30"
workthumb =  "/img/mr-strong-thumb.png"
thumb =  "/img/mr-strong-thumb-600.png"
tags = ["realistic", "mascot"]
client = "Pulvitec, Pidilite"
description = "A Mascot design and illustration done for Pulvitec Brazil."
keywords = ["Mascot design", "Illustration", "mr strong", "pulvitec", "brazil", "adhesive", "super brilo"]
slug = "mascot-for-pulvitec-brazil"
+++

I was commissioned to refresh the mascot of Pulvitec Brazil's - [Mr.
Strong](http://mrstrong.com.br/mrstrong) line of products. The mascot
was earlier in 2d graphic style, [Pulvitec](http://www.pulvitec.com.br/)
wanted give the mascot a new refreshed 3d look, without changing the
exisiting features and design of the mascot.

![pulvitec Mr strong mascot](/img/mr-strong/mr-strong-a.png)

![pulvitec Mr strong mascot](/img/mr-strong/mr-strong-b.png)

![pulvitec Mr strong mascot](/img/mr-strong/mr-strong-c.png)

![pulvitec Mr strong mascot's puppy](/img/mr-strong/puppy.png)

Artwork copyright and trademarks owned by Pulvitec Industries of Brazil.
