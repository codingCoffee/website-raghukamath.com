+++
title = "Dhobi Ghat"
date = "2015-11-30T12:00:00+05:30"
workthumb = "/img/dthumb-300x300.jpg"
thumb = "/img/dthumb-600.jpg"
tags = ["graphic", "personal"]
description = "Illustration of largest open air laundry in Asia"
keywords = ["dobhi ghat", "open air laundry", "mumbai", "dobhi ghat illustrations"]
+++

## Indian open air laundry

[Dhobi Ghat](https://en.wikipedia.org/wiki/Dhobi_Ghat) is a 150 year old
place in Mumbai where hundreds of washer men collect dirty laundry, wash
and return it, neatly ironed, to swank homes of elites living in south
Mumbai. It is the worlds largest open air laundry. It's a fascinating
spectacle, looking down on row upon row of open-air concrete wash pens,
each fitted with its own flogging stone, while Mumbai's dhobis (around
200 dhobi families work together here) relentlessly pound the dirt from
the city's garments in a timeless tradition

![dhobi Ghat - Washing](/img/dobhi/d1.jpg)

![dhobi Ghat ironing](/img/dobhi/d2.jpg)

![Dhobi ghat drying](/img/dobhi/d3.jpg)

![dhobi Ghat - sorting](/img/dobhi/d4.jpg)

![Dhobi Ghat - rinsing](/img/dobhi/d5.jpg)

![dhobi Ghat - brushing](/img/dobhi/d6.jpg)

![dhobi Ghat - porting](/img/dobhi/d7.jpg)

![dhobi Ghat - washing](/img/dobhi/d8.jpg)
