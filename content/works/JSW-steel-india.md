+++
title = "JSW Steel India"
date = "2017-08-17T19:18:00+05:30"
workthumb = "/img/jsw-steel-thumb-300x300.jpg"
thumb = "/img/jsw-steel-thumb-600.jpg"
tags = ["3D"]
client = "JSW Steel"
desciption = "A map of India created with steel elements"
keywords = ["3d", "blender", "india", "illustrations", "JSW steel", "Steel building", "India steel"]
+++

This commisioned illustration was for [JSW Steel
India](https://en.wikipedia.org/wiki/JSW_Steel_Ltd) , they wanted a map
of India created from steel structures, to denote that steel powers the
infrastructure in India. This was a collaboration with Prasad Kulkarni
of Ogilvy & Mather Mumbai. These illustrations are done in
[Blender](https://www.blender.org/).

![JSW Steel India Illustration](/img/jsw/jsw-steel-illustration-03.jpg)

![JSW Steel India Illustration](/img/jsw/jsw-steel-illustration-02.jpg)

![JSW Steel India Illustration](/img/jsw/jsw-steel-illustration-01.jpg)

Artwork copyright and trademarks owned by JSW Steel India Ltd.
