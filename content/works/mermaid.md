+++
title = "Mermaid"
date = "2015-12-15T12:15:00+05:30"
workthumb = "/img/mermaid.jpg"
thumb = "/img/mermaid-600.jpg"
tags = ["realistic", "personal"]
description = "Mermaid illustration done in krita"
keywords = ["mermaid", "floss painting", "digital painting tutorial", "krita timelapse", "timelapse", "floss art", "krita"]
draft = "true"
+++

An Illustration done in leisure.

![Mermaid Illustration](/img/mermaid-full.jpg)

![Mermaid Illustrations detail](/img/mermaid-detail.jpg)
