+++
title = "Festival of lights"
date  =  "2016-12-24T15:49:00+05:30"
workthumb =  "/img/festival-of-light-thumb.png"
thumb =  "/img/festival-of-light-thumb-600.png"
tags  =  ["realistic"]
client   =  "Krita foundation"
description =  "A Illustration done for Krita Artbook submission"
keywords = ["floss painting", "digital painting tutorial", "krita timelapse", "timelapse", "floss art", "made in krita", "artbook", "krita"]
slug = "festival-of-lights"
+++

An illustration done for first Krita artbook by Krita foundation called
**Made with Krita**. This artbook will be given as a reward for backers
of the Krita\'s kickstarter
[campaign](https://www.kickstarter.com/projects/krita/krita-2016-lets-make-text-and-vector-art-awesome).

You can order the artbook from the krita foundation through this
[link](https://gumroad.com/l/LLUIt#).

![Festival of light Illustration](/img/festival-of-light.png)

I have also shared a process video in my journal
[here](https://raghukamath.com/journal/festival-of-lights-timelapse-painting).
