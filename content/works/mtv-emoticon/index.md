+++
title = "Mtv Biryani Emoticon Posters"
date = 2019-11-24T11:00:34+05:30
client = "Viacom18"
thumb = "/works/mtv-emoticon/mtv-thumb-600.png"
workthumb = "/works/mtv-emoticon/mtv-thumb-300.png"
tags = ["graphic"]
keywords = ["Viacom", "Mtv India", "Biryani Emoticon", "Biryani", "Chicken Illustration", "goats", "emoticon"]
description = "Poster Illustrations for MTV India's Biryani Emoticon Campaign"
+++

My [studio](https://emblik.studio) was commissioned by Viacom to make posters for their campaign demanding emoticon for Biryani dish. These are some Illustrations that I did for them. These were done along the style of propaganda posters but I think in the end it drifted towards normal style a bit.

![Chicken Biryani Illustration](biryani-chicken.png)  

![Chicken Biryani Details](biryani-chicken-01.png)  

![Mutton Biryani Illustration](biryani-mutton.png)  

![Mutton Biryani Illustration details](biryani-mutton-01.png)  

Illustrations adapted in layout for the emailer created by the MTV India Design team.

![Chicken Biryani layout](mtv-chicken.png)  

![Mutton Biryani layout](mtv-mutton.png)  


Artwork is under copyright of Viacom18.
