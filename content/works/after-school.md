+++
title = "After School"
date = 2019-02-25T13:58:34+05:30
thumb = "/img/after-school-thumb-600.png"
workthumb = "/img/after-school-thumb-300.png"
tags = ["personal", "cc-by"]
keywords = ["after school", "nostalgia", "krita", "catching tadpoles", "tadpoles", "indian school boy"]
description = "A nostalgic memory of catching and playing with tadpoles, while returning from school. the paddy fields used to have millions of them. Exploring some brushes and painterly style in Krita."
draft = "true"
+++

A nostalgic memory of catching and playing with tadpoles, while returning from school. the paddy fields used to have millions of them. Exploring some brushes and painterly style in Krita.
The joy of mixing color on canvas is truly amazing and Krita highlights that aspect very much even in digital space.

![digital painting dipicting a boy catching tadpole besides a paddy field](/img/after-school.png)

Artwork is under CC-BY-SA-4.0, source file can be downloaded from - [here](https://box.raghukamath.com/cloud/index.php/s/ZL4ZxsEyr6DDHDf).


