+++
title = "Maharaja – concept for new story"
date = "2015-11-30T18:47:00+05:30"
workthumb = "/img/thumb2-300x300.jpg"
thumb = "/img/process-maharaja.jpg"
tags = ["realistic", "personal", "CC-BY-SA"]
description = "Maharaja – a character concept from a story that I am developing. This is done in FLOSS painting software Krita 2.9 on a linux system."
keywords = ["maharaja", "india illustration", "mumbai", "linux digital art", "floss digital painting", "dictator portrait", "protrait illustrations"]
slug = "maharaja-concept-for-new-story"
+++

Maharaja -- a character concept from a story that I am developing. This
is done in FLOSS painting software Krita 2.9 on a linux system.

![maharaja](/img/maharaja.jpg)
