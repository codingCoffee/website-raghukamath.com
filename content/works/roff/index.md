+++
title = "Roff Annual Diary"
date = 2016-07-21T21:00:34+05:30
client = "pidilite"
thumb = "/works/roff/roff-thumb-600.png"
workthumb = "/works/roff/roff-thumb-300.png"
tags = ["realistic", "mascot"]
keywords = ["pidilite", "roff brand", "roff alligator illustration", "pidilite roff glue"]
description = "Mascot refresh and Cover Illustrations for Annual Diaries, done for Roff brand of Pidilite Industries India"
+++

I collaborated with the in-house design team of Pidilite Industries to create cover Illustrations of the annual diaries of the year 2017 and 2019.
This was for their Roff brand of adhesives which are used for fixing tiles. The Alligator mascot was shown in various poses and styles for the cover.

![Alligator putting the tiles in masons clothes - pidilite](roff-01.png)  

![Detail of the alligator - pidilite](roff-02.png)  

And alternative layout and pose for the same diary.

![An alternative pose of the alligator- pidilite](roff-03.png)  

Illustration done for the 2017 diary. here the alligator is rendered as if it was made of the tiles. The adhesive is used for fixing tiles on the walls.

![Illustration for the 2017 diary - pidilite](roff-04.png)  

![Detail of the 2017 diary illustration - pidilite](roff-05.png)  

Artwork is under copyright of Pidilite Industries.
