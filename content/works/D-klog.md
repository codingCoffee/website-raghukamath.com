+++
title = "Illustration for Pidilite – Dklog"
date = 2015-11-30T18:27:00+05:30
workthumb = "/img/D-klog-thumb-300x300.jpg"
thumb = "/img/D-klog-thumb-600.jpg"
tags = ["realistic"]
client = "pidilite"
keywords = ["Pidilite", "illustrations", "d-klog", "pidilite india", "krita", "linux", "floss painting"]
slug = "illustration-for-pidilite-dklog"
description = "An Illustration done for Pidilite D-klog. This illustration was completely done in Krita on a linux system"
+++

An Illustration done for Pidilite D-klog. This illustration was
completely done in Krita on a linux system

![D-klog Pidilite Illustrations](/img/D-klog.jpg)

![D-klog pidilite Illustrations](/img/D-klog-closeup.jpg)
