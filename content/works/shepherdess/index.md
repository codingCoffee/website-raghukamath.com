+++
title = "Shepherdess"
date = 2019-09-21T21:00:34+05:30
thumb = "/works/shepherdess/shepherdess-thumb-600.png"
workthumb = "/works/shepherdess/shepherdess-thumb-300.png"
tags = ["commission", "cc-by-sa"]
keywords = ["silk worm", "shepherdess", "krita", "lady", "indian", "small world"]
description = "A commission artwork done for a friend. It is about a shepherdess who rears silkworms for a living."
+++

A small tribal shepherdess sitting idly on a twig, dreaming about the happy days yet to come. While she sits calmly, the meek silkworms slowly devour an entire bush of mulberry. It is the mulberry of this giant forest that helps her tribe, to produce one of the finest silk that one can get on this land.

A commission done for a dear friend.

![digital painting dipicting a Shepherdess sitting idly on a twig](shepherdess.jpg)  

![Some details of the painting - shepherdess](shepherdess-01.png)  

![Some details of the painting - shepherdess](shepherdess-02.png)  


Artwork is under CC-BY-SA-4.0, source file can be downloaded from - [here](https://box.raghukamath.com/cloud/index.php/s/QLNSMb4BSKqq53o).


