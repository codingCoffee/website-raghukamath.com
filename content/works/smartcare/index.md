+++
title = "Asian Paints Smart Care"
date = 2019-11-21T21:00:34+05:30
thumb = "/works/smartcare/smartcare-thumb-600.png"
workthumb = "/works/smartcare/smartcare-thumb-300.png"
tags = ["commission", "realistic"]
client = "Asian Paints"
keywords = ["asian paints", "Illustrations", "smart care", "exterior emulsion paint", "painter illustration"]
description = "A commission done for Asian Paints for their exterior emulsion press and billboard ads"
+++

I collaborated with Ogilvy India to create an illustration for billboard and press ads of [Asian Paints](https://www.asianpaints.com/).

![Digital painting for Asian Paints](smart-care-001.jpg)  

The Illustration were adapted to press and billboard layout by Ogilvy design team.

![Illustration adapted to press layout](smart-care-002.jpg)  

![Illustration adapted for billboard layout](smart-care-003.jpg)  

Artwork is under copyright of Ogivly and Asian Paints Ltd.


