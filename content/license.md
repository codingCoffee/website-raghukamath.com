---
title: License
type: page
layout: static
---

## License for the commissioned work

All the original content and artwork created by me for any individual,
company or organization, published on this website are explicitly under
copyright policy and license terms of the respective owners, unless
otherwise specifically stated. I do not grant any permission to share
copy or redistribute the content and artwork.

## Personal artwork and content

All the original content and images in this website created by me, unless specifically stated, are licensed under the - Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). You can share the artwork as long as you follow the following terms:

-   **Attribution** --- You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
-   **Share Alike** --- If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

**No additional restrictions** --- You may not apply legal terms or
technological measures that legally restrict others from doing anything
the license permits.

You can read the license document in detail [here.](https://creativecommons.org/licenses/by-sa/4.0/legalcode)

## License for code

All the code shared in this website is under [General Public License Version-3](http://www.gnu.org/licenses/gpl.html), a copy of license is [here](/downloads/License-gnugplv3.txt).

## Fonts

Fonts used in this website are -
[Libre Baskerville](https://fontlibrary.org/en/font/libre-baskerville) designed by Pablo Impallari and Rodrigo Fuenzalida and [Lato](http://www.lukaszdziedzic.eu/#Lato) designed by [Łukasz Dziedzic](http://www.lukaszdziedzic.eu). Both the fonts are licensed under [Open Font License](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web).
