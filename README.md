## Raghukamath.com
My personal website built with the help of hugo.


## Images
I have not uploaded the images directory as it is somewhat bigger in size

## License
As stated in license file all the code is under GNUgpl-v3 licence and the original content like images, articles photographs are under CC BY-SA 4.0 license
Part of hugo code are under [MIT](https://opensource.org/licenses/MIT).
